/*eslint no-unused-vars: "off" */
let express = require("express")
let path = require('path');
let cookieParser = require('cookie-parser');
let bodyParser = require("body-parser")
let logger = require('morgan');
let favicon = require("serve-favicon")
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
const movie = require('./routes/movie');
const  show = require('./routes/show');
const review = require('./routes/review');

let app = express();



app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
  if (req.method === 'OPTIONS') {
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    res.header('Access-Control-Allow-Methods', '*');
    return res.status(200).json({});
  };
  next();

});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, "public")))
if (process.env.NODE_ENV !== "test") {
  app.use(logger("dev"))
}

app.use('/', indexRouter);
app.use('/users', usersRouter);

app.get('/movie',movie.findAllMovies);
app.get('/movie/:id',movie.findOneByID);
app.post('/movie', movie.addMovie);
app.put('/movie/:id/purchase', movie.purchaseMovie);
app.delete('/movie/:id',movie.deleteMovie);

app.get('/show',show.findAllShows);
app.get('/show/:id',show.findOneByID);
app.post('/show', show.addShow);
app.put('/show/:id/purchase', show.purchaseShow);
app.delete('/show/:id', show.deleteShow);

app.get('/review',review.findAllReviews);
app.get('/review/:id',review.findOneByID);
app.get('/review/author/:author',review.findByAuthor)
app.post('/review/write',review.addReviewMovie);
app.put('/review/:id/like',review.likeReview);
app.delete('/review/:id',review.deleteReview);
app.put('/review/edit/:id',review.editReview);
app.get('/review/movie/:titleID',review.findMovieReview);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  let err = new Error("Not Found")
  err.status = 404
  next(err)
})

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
